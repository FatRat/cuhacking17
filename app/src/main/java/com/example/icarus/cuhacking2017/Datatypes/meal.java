package com.example.icarus.cuhacking2017.Datatypes;

import com.example.icarus.cuhacking2017.Tools.intToTime;

import java.util.ArrayList;

public class meal {
    int timeDesignation;
    String mealName;
    intToTime ct = new intToTime();

    ArrayList<food> foodList = new ArrayList<food>();
    public meal(int setTime, String setMealName){
        timeDesignation = setTime;
        mealName = setMealName;
    }
    public String getMealName(){
        return mealName;
    }
    public void setTimeDesignation(int setTime){
        timeDesignation = setTime;
    }
    public int getIntTime(){
        return timeDesignation;
    }
    public String getTime(){
        return ct.convert(timeDesignation);
    }
    public void addItem(food item){
        foodList.add(item);
    }
    public food getItem(int index){
        return foodList.get(index);
    }
    public int getSize(){
        return foodList.size();
    }
}
