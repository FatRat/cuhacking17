package com.example.icarus.cuhacking2017.Tools;

import android.content.Context;

import com.example.icarus.cuhacking2017.Datatypes.food;
import com.example.icarus.cuhacking2017.Datatypes.meal;
import com.example.icarus.cuhacking2017.R;

import java.io.InputStream;
import java.util.Scanner;

public class loadMeal{
    int segment;
    int tempCal;
    int tempWeight;
    char tempGroup;
    String tempName;
    String tempShortDesc;
    StringBuilder tempBuffer;
    food tempFood;

    Context localContext;

    public loadMeal(Context setLocalContext){
        localContext = setLocalContext;
    }
    public meal load(int mealTime, String mealName, String fileName){
        String loadedFile;
        meal tempMeal = new meal(mealTime, mealName);
        loadedFile = readFile(fileName);
        tempBuffer = new StringBuilder();

        if(loadedFile != null){
            for (int i = 0; i < (loadedFile.length()); i++){
                if(loadedFile.charAt(i) == ':') {
                    cleartemps();
                }else if(loadedFile.charAt(i) == ','){
                    switch (segment) {
                        case 0:
                            tempCal = Integer.parseInt(tempBuffer.toString());
                            tempBuffer.setLength(0);
                            break;
                        case 1:
                            tempWeight = Integer.parseInt(tempBuffer.toString());
                            tempBuffer.setLength(0);
                            break;
                        case 2:
                            tempGroup = tempBuffer.charAt(0);
                            tempBuffer.setLength(0);
                            break;
                        case 3:
                            tempName = usts(tempBuffer.toString());
                            tempBuffer.setLength(0);
                            break;
                        case 4:
                            tempShortDesc = usts(tempBuffer.toString());
                            tempBuffer.setLength(0);
                            break;
                        default:
                            break;
                    }
                    segment++;
                }else if(loadedFile.charAt(i) == '<'){
                    tempFood = new food(tempCal, tempWeight, tempGroup, tempName, tempShortDesc);
                    tempMeal.addItem(tempFood);
                }else{
                    tempBuffer.append(loadedFile.charAt(i));
                }
            }
        }else{
            System.out.println("welp, file didn't load (STRING = NULL)");
        }

        return tempMeal;
    }
    private void cleartemps(){
        tempCal = 0;
        tempWeight = 0;
        segment = 0;
        tempGroup = '\u0000';
        tempName = "";
        tempShortDesc = "";
        tempBuffer.setLength(0);
    }
    private String readFile(String loadFile){
        Scanner sIn = new Scanner(localContext.getResources().openRawResource(localContext.getResources().getIdentifier(loadFile, "raw", localContext.getPackageName())));
        StringBuilder readInString = new StringBuilder();

        try {
            while(sIn.hasNext()){
                readInString.append(sIn.next());
            }
        }finally{
            sIn.close();
        }
        return readInString.toString();
    }

    private String usts(String inString){
        char[] tCharArr = inString.toCharArray();

        for(int i = 0; i < tCharArr.length; i++){
            if(tCharArr[i] == '_'){
                tCharArr[i] = ' ';
            }
        }

        return String.valueOf(tCharArr);
    }
}
