package com.example.icarus.cuhacking2017.Tools;

public class intToTime {
    public String convert(int inTime) {
        StringBuilder outTime = new StringBuilder();
        String tTime;
        String mTime;
        int intmCalc;
        int mOut;

        tTime = String.valueOf(inTime);
        int i = 0;
        intmCalc = 1;
        while ((i <= 24) && intmCalc >= -100){
            intmCalc = inTime - (i * 100);
            if (intmCalc < 0 && intmCalc >= -100) {
                if (((i - 1) < 10)) {
                    outTime.append("0" + (i - 1));
                } else {
                    outTime.append(i - 1);
                }
            }
            i++;
        }
        outTime.append(":");
        mTime = tTime.substring(tTime.length() - 2, tTime.length());
        mOut = (int)Math.round(60 * Double.parseDouble("0." + mTime));
        if(mOut < 9){
            outTime.append("0" + String.valueOf(mOut));
        }else{
            outTime.append(String.valueOf(mOut));
        }


        return outTime.toString();
    }
}
