package com.example.icarus.cuhacking2017.Datatypes;

/**
 * Created by icarus on 04/03/17.
 */

public class food {

    private int cal;
    private int weight;
    private char foodGroup;
    private String itemName;
    private String shortDesc;

    public food(int setCal,int setWeight, char setFoodGroup, String setItemName, String setShortDesc){
        cal = setCal;
        weight = setWeight;
        foodGroup = setFoodGroup;
        itemName = setItemName;
        shortDesc = setShortDesc;
    }

    public int getCal() {
        return cal;
    }
    public int getWeight() {
        return weight;
    }
    public char getFoodGroup() {
        return foodGroup;
    }
    public String getItemName() {
        return itemName;
    }
    public String getShortDesc() {
        return shortDesc;
    }
}
