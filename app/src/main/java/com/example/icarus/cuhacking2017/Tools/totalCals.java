package com.example.icarus.cuhacking2017.Tools;

import com.example.icarus.cuhacking2017.Datatypes.meal;
import java.util.ArrayList;

/**
 * Created by icarus on 05/03/17.
 */

public class totalCals {
    public int sumCals(ArrayList<meal> mealList){
        int sum = 0;
        for (int i = 0; i < mealList.size(); i++){
            for (int q = 0; q < mealList.get(i).getSize(); q++){
                sum += mealList.get(i).getItem(q).getCal();
            }
        }

        return sum;
    }

}
