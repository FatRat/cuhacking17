package com.example.icarus.cuhacking2017;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;
import com.example.icarus.cuhacking2017.Tools.intToTime;
import com.example.icarus.cuhacking2017.Tools.loadMeal;
import com.example.icarus.cuhacking2017.Datatypes.meal;
import com.example.icarus.cuhacking2017.Tools.mealListSort;
import com.example.icarus.cuhacking2017.Tools.totalCals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class mainscreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    loadMeal mealLoader;
    meal tempMeal;
    ListView listView;

    boolean onParentMenu;
    mealListSort mealSort = new mealListSort();
    ArrayList<meal> mealList = new ArrayList<meal>();
    totalCals tCals = new totalCals();

    TextView dateText;

    String currentDate;

    intToTime ct = new intToTime();
    int sumCals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainscreen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        onParentMenu = true;
        mealLoader = new loadMeal(this);

        //these could be pulled from a server with a loop, setup an account
        //for the specific user
        tempMeal = mealLoader.load(1500, "Snack", "snack");
        mealList.add(tempMeal);
        tempMeal = mealLoader.load(1250, "Lunch", "lunch");
        mealList.add(tempMeal);
        tempMeal = mealLoader.load(700, "Breakfast", "breakfast");
        mealList.add(tempMeal);
        tempMeal = mealLoader.load(1950, "Dinner", "dinner");
        mealList.add(tempMeal);

        loadMealList();

        sumCals = tCals.sumCals(mealList);

        SimpleDateFormat dateF = new SimpleDateFormat("MMMM dd yyyy");
        Calendar cal = Calendar.getInstance();

        currentDate = dateF.format(cal.getTime());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (onParentMenu){
                    int itemPos = position;
                    String itemVal = (String) listView.getItemAtPosition(position);
                    onParentMenu = false;
                    ArrayList<String> thingsList = new ArrayList<String>();

                    for (int i = 0; i < (mealList.get(itemPos).getSize()); i++){
                            thingsList.add(mealList.get(itemPos).getItem(i).getItemName());
                            int curCal = mealList.get(itemPos).getItem(i).getCal();
                            int curWeight = mealList.get(itemPos).getItem(i).getWeight();
                            String curDesc = mealList.get(itemPos).getItem(i).getShortDesc();
                            char curGroup = mealList.get(itemPos).getItem(i).getFoodGroup();
                            thingsList.add(curDesc + " - Cal: " + curCal + " Portion: " + curWeight + "g Group: " + curGroup);
                            if(!(i == (mealList.get(itemPos).getSize()) - 1)){
                                thingsList.add("");
                            }
                    }
                    populateListView(thingsList);
                }
            }
        });


        }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        loadMealList();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainscreen, menu);
        dateText = (TextView)findViewById(R.id.textView4);

        if (currentDate != null){
            dateText.setText(currentDate + "   \nCalorie Goal: " + sumCals);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        ArrayList<String> blank = new ArrayList<String>();

        if (id == R.id.nav_meals) {
           loadMealList();
        }else if (id == R.id.nav_goals) {
           populateListView(blank);
        }else if (id == R.id.nav_communicate){
            populateListView(blank);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void loadMealList(){
        onParentMenu = true;
        ArrayList<String> namesList = new ArrayList<String>();
        mealList = mealSort.sort(mealList);
        for (int i = 0; i < mealList.size(); i++){
            namesList.add(mealList.get(i).getMealName() + "  -  " + mealList.get(i).getTime());
        }

        populateListView(namesList);
    }

    private void populateListView(ArrayList<String> itemsList){
        listView = (ListView) findViewById(R.id.mainListView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1,itemsList);

        listView.setAdapter(adapter);
    }

}
