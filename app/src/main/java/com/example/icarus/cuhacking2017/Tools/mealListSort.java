package com.example.icarus.cuhacking2017.Tools;

import com.example.icarus.cuhacking2017.Datatypes.meal;

import java.util.ArrayList;


public class mealListSort {
    private ArrayList<meal> usMealList = new ArrayList<meal>();
    boolean complete = false;
    int lastVal = 0;
    meal tempMeal;

    public ArrayList<meal> sort(ArrayList<meal> inputML){
        usMealList = inputML;
        tempMeal = null;
        complete = false;
        int noError = 0;

        while(!complete){
            for(int z = 0; z < inputML.size(); z++){
                lastVal = inputML.get(0).getIntTime();
                for(int q = 1; q < inputML.size(); q++){
                    if (inputML.get(q).getIntTime() < lastVal){
                        tempMeal = inputML.get(q-1);
                        inputML.set(q-1,inputML.get(q));
                        inputML.set(q,tempMeal);
                    }
                    noError++;
                    if (noError >= inputML.size()){
                        complete = true;
                    }
                    lastVal = inputML.get(q).getIntTime();
                }
            }

        }
        return inputML;
    }

}
